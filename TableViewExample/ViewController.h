//
//  ViewController.h
//  TableViewExample
//
//  Created by LNT Chirag on 6/17/15.
//  Copyright (c) 2015 LNT Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableVIew;

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) NSMutableArray *sportsArray;
@property (strong, nonatomic) NSMutableArray *playersArray;
- (IBAction)startEditing:(id)sender;
- (IBAction)indexChanged:(UISegmentedControl *)sender;

@end


//
//  AppDelegate.h
//  TableViewExample
//
//  Created by LNT Chirag on 6/17/15.
//  Copyright (c) 2015 LNT Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


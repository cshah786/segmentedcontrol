//
//  ViewController.m
//  TableViewExample
//
//  Created by LNT Chirag on 6/17/15.
//  Copyright (c) 2015 LNT Chirag. All rights reserved.
//

#import "ViewController.h"
#import "FirstCustomTableViewCell.h"
#import "SecondCustomTableViewCell.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize sportsArray,playersArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //NSMutableArray *sportsArray;
    self.tableVIew.allowsSelectionDuringEditing = YES;
    
    
    sportsArray = [NSMutableArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:@"Cricket", @"Name",@"CellType1",@"Type",nil],[NSDictionary dictionaryWithObjectsAndKeys:@"Football", @"Name",@"CellType2",@"Type",nil],[NSDictionary dictionaryWithObjectsAndKeys:@"VolleyBall", @"Name",@"CellType2",@"Type",nil],[NSDictionary dictionaryWithObjectsAndKeys:@"Hockey", @"Name",@"CellType1",@"Type",nil],[NSDictionary dictionaryWithObjectsAndKeys:@"Pool", @"Name",@"CellType2",@"Type",nil], nil];
    
    //sportsArray = [NSMutableArray arrayWithObjects:@"Cricket",@"Football",@"VolleyBall",@"Hockey", nil];
    
    playersArray = [NSMutableArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:@"Sachin", @"Name",@"CellType1",@"Type",nil],[NSDictionary dictionaryWithObjectsAndKeys:@"Ronaldo", @"Name",@"CellType2",@"Type",nil], nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section==0) {
        return @"First";
    }
    return @"Second";
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
    {
        return [sportsArray count];
    }
    else{
        return [playersArray count];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        NSDictionary *dictionary = [sportsArray objectAtIndex:indexPath.row];
        
        if ([[dictionary objectForKey:@"Type"] isEqualToString:@"CellType1"]) {
            static NSString *cellIdentifier = @"Cell";
            FirstCustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                cell = [[FirstCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                
            }
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//                NSURL *url = [NSURL URLWithString:@"http://bit.ly/nUX01h"];
//                NSURLRequest *req = [NSURLRequest requestWithURL:url];
//                [NSURLConnection sendAsynchronousRequest:req
//                                                   queue:[NSOperationQueue currentQueue]
//                                       completionHandler:
//                 ^(NSURLResponse *res, NSData *data, NSError *err) {
//                     // Convert the data to a UIImage
//                     UIImage *image = [UIImage imageWithData:data];
//                     
//                     // Scale the image
//                     UIImage *thumbImage = nil;
//                     CGSize newSize = CGSizeMake(90, (90 / image.size.width) * image.size.height);
//                     UIGraphicsBeginImageContext(newSize);
//                     [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
//                     thumbImage = UIGraphicsGetImageFromCurrentImageContext();
//                     UIGraphicsEndImageContext();
                UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                [indicator startAnimating];
                [indicator setCenter:cell.imageView.center];
                [cell.imageView addSubview:indicator];
                 UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://bit.ly/nUX01h"]]];
                     dispatch_async(dispatch_get_main_queue(), ^{
                          [indicator stopAnimating];
                         cell.imageDetail.image = image;
                        
                     });
             //    }];
            });

            cell.labelText.text = [[sportsArray objectAtIndex:indexPath.row] objectForKey:@"Name"];
        //    cell.imageDetail.image =[UIImage imageNamed:@"facebook.png"];
            return cell;
        }
        else
        {
            static NSString *cellIdentifier = @"CustomCell1";
            SecondCustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                cell = [[SecondCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                
            }
            
            cell.textLabel.text = [[sportsArray objectAtIndex:indexPath.row] objectForKey:@"Name"];
            return cell;
        }
    }
    else
    {
        NSDictionary *dictionary = [playersArray objectAtIndex:indexPath.row];
        
        if ([[dictionary objectForKey:@"Type"] isEqualToString:@"CellType1"]) {
            static NSString *cellIdentifier = @"Cell";
            FirstCustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                cell = [[FirstCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                
            }
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
              UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://bit.ly/nUX01h"]]];
                     
                     // Scale the image
//                     UIImage *thumbImage = nil;
//                     CGSize newSize = CGSizeMake(90, (90 / image.size.width) * image.size.height);
//                     UIGraphicsBeginImageContext(newSize);
//                     [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
//                     thumbImage = UIGraphicsGetImageFromCurrentImageContext();
//                     UIGraphicsEndImageContext();
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         cell.imageDetail.image = image;
                     });
                
            });
            cell.labelText.text = [[playersArray objectAtIndex:indexPath.row] objectForKey:@"Name"];
           
          //  cell.imageDetail.image =[UIImage imageNamed:@"facebook.png"];
            return cell;
        }
        else
        {
            static NSString *cellIdentifier = @"CustomCell1";
            SecondCustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                cell = [[SecondCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                
            }
            
            cell.textLabel.text = [[playersArray objectAtIndex:indexPath.row] objectForKey:@"Name"];
  
            return cell;
        }
    }
    
    //   // if(indexPath.row ==0 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3)
    //   if(indexPath.section == 0)
    //    {
    //   static NSString *cellIdentifier = @"Cell";
    //    FirstCustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    //    if (cell == nil) {
    //
    //        cell = [[FirstCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    //
    //    }
    //
    //        cell.labelText.text = [sportsArray objectAtIndex:indexPath.row];
    //   // UILabel *sportsLabel = (UILabel *)[cell viewWithTag:101];
    //   // sportsLabel.text = [sportsArray objectAtIndex:indexPath.row];
    //  //   sportsLabel.text = @"Cricket";
    //
    //    //UILabel *sportsPlayer = (UILabel *)[cell viewWithTag:102];
    //  //  sportsPlayer.text = [sportsArray objectAtIndex:indexPath.row];
    //
    //    //    sportsPlayer.text = @"Sachin";
    //
    // //   cell.textLabel.text = [_sportsArray objectAtIndex:indexPath.row];
    //    return cell;
    //
    //    }
    //
    //
    //    else
    //    {
    //        static NSString *cellIdentifier = @"CustomCell1";
    //        SecondCustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    //        if (cell == nil) {
    //
    //            cell = [[SecondCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    //
    //        }
    //
    //
    //
    //        cell.textLabel.text = [playersArray objectAtIndex:indexPath.row];
    //        return cell;
    //
    //    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        return 100.0;
    }
    
    else{
        return 80.0;
    }
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        if(indexPath.section == 0)
        {
            [sportsArray removeObjectAtIndex:indexPath.row];
            [self.tableVIew beginUpdates];
            [self.tableVIew deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableVIew endUpdates];
            
            
        }
        
        else{
            [playersArray removeObjectAtIndex:indexPath.row];
            [self.tableVIew beginUpdates];
            [self.tableVIew deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableVIew endUpdates];
        }
        
    }
}

- (BOOL) tableView: (UITableView *) tableView canEditRowAtIndexPath: (NSIndexPath *) indexPath;
{
    return YES;
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewRowAction *moreAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Insert" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        // show UIActionSheet
        
        NSLog(@"Insert Button Has been clicked");
        
        
        
        
    }];
    moreAction.backgroundColor = [UIColor greenColor];
    
    UITableViewRowAction *flagAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        // flag the row
        
        NSLog(@"Delete Button Has been clicked");
        
            if(indexPath.section == 0)
            {
                [sportsArray removeObjectAtIndex:indexPath.row];
                [self.tableVIew beginUpdates];
                [self.tableVIew deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                [self.tableVIew endUpdates];
                
                
            }
            
            else{
                [playersArray removeObjectAtIndex:indexPath.row];
                [self.tableVIew beginUpdates];
                [self.tableVIew deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                [self.tableVIew endUpdates];
            }
    }];
    flagAction.backgroundColor = [UIColor redColor];
    
    return @[moreAction, flagAction];
}


- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    if(sourceIndexPath.section == 0)
    {
        NSString *stringToMove = self.sportsArray[sourceIndexPath.row];
        [self.sportsArray removeObjectAtIndex:sourceIndexPath.row];
        [self.sportsArray insertObject:stringToMove atIndex:destinationIndexPath.row];
    }
    else{
        NSString *stringToMove = self.playersArray[sourceIndexPath.row];
        [self.playersArray removeObjectAtIndex:sourceIndexPath.row];
        [self.playersArray insertObject:stringToMove atIndex:destinationIndexPath.row];
    }
}

- (IBAction)startEditing:(id)sender
{
    self.editing =YES;
}

- (IBAction)indexChanged:(UISegmentedControl *)sender
{
   
        switch (self.segmentedControl.selectedSegmentIndex)
        {
            case 0:
                NSLog(@"First Switch Clicked");
                
                break;
            case 1:
                                  NSLog(@"Second Switch Clicked");
                break; 
            default: 
                break; 
        
    }
}
@end

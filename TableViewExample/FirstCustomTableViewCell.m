//
//  FirstCustomTableViewCell.m
//  TableViewExample
//
//  Created by LNT Chirag on 6/26/15.
//  Copyright (c) 2015 LNT Chirag. All rights reserved.
//

#import "FirstCustomTableViewCell.h"

@implementation FirstCustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
